package com.zenika.academy.videogames;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoGamesCollectionApplication {

    public static void main(String[] args) {
        SpringApplication.run(VideoGamesCollectionApplication.class, args);
    }

}
